import React, { useState, useEffect } from "react";
import axios from 'axios';
import { saveAs } from 'file-saver';

const WalletInitialization = () => {
  const [wallet, setWallet] = useState({
    name: "",
    balance: 0,
  });
  const [balance, setBalance] = useState({
    name: "",
    balance: 0,
  });
  const [data, setData] = useState([]);

  const [transactionAmount, setTransactionAmount] = useState('');
  const [transactionType, setTransactionType] = useState('CREDIT');

  const handleSubmit = async (e) => {
    // make a post request to the /wallet endpoint with the wallet data
    // on success, save the returned wallet id to local storage
    // and update the wallet state with the returned data

    e.preventDefault();
    console.log("checking default event value ", e.preventDefault())

    console.log("checking wallets details", wallet);
    try {
      const response = await axios.post('https://highlevel-wallet.onrender.com/setup', wallet);
      console.log("api response data", response.data);
      localStorage.setItem('walletId', response.data.id);
      setWallet({
        ...wallet,
        name: response.data.name,
        balance: response.data.balance
      });

    } catch (error) {
      console.error(error);
    }
  };


  const handleSubmit1 = async (event) => {
    event.preventDefault();
    console.log("checking transaction transactionAmount values 1", +transactionAmount)
    console.log("checking transaction description values 1", transactionType)

    try {
      const walletId = localStorage.getItem('walletId');

      const response = await axios.post(`https://highlevel-wallet.onrender.com/transact/${walletId}`, {
        amount: transactionType === 'CREDIT' ? +transactionAmount : -transactionAmount,
        description: transactionType
      });
      console.log("after transaction api response",response.data);
      setBalance({
        ...balance,
        name: response.data.name,
        balance: response.data.balance
      });
    } catch (error) {
      console.error(error);
    }
  };


  useEffect(() => {
    const walletId = localStorage.getItem('walletId');
    const fetchWallet = async () => {
      const response = await axios.get(`https://highlevel-wallet.onrender.com/wallet/${walletId}`);
      const transactionData = await axios.get(`https://highlevel-wallet.onrender.com/transactions/?walletId=${walletId}`);

      console.log(response, "checking wallet api response")
      console.log(transactionData, "checking all transactionData api response")

      setBalance({
        ...balance,
        name: response.data.name,
        balance: response.data.balance
      });
      setData(transactionData.data)
    };
    fetchWallet();
  }, [balance]);



  const handleExport = () => {
    const csvData = data.map((item) => [item._id, item.amount, item.description, item.created_at].join(',')).join('\n');
    const blob = new Blob([csvData], { type: 'text/csv;charset=utf-8;' });
    saveAs(blob, 'data.csv');
  };

  const handleInputChange = (event) => {
    console.log("cheking events value", event);
    setWallet({
      ...wallet,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <div className="container">
      <h1>Wallet Initialization</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Username:</label>
          <input
            type="text"
            id="name"
            name="name"
            value={wallet.name}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="balance">Initial Balance:</label>
          <input
            type="number"
            id="balance"
            name="balance"
            value={wallet.balance}
            onChange={handleInputChange}
          />
        </div>
        <button type="submit">Submit</button>
      </form>

      <div>
        <h1>Welcome, {balance.name}</h1>
        <p>Your current balance is {balance.balance}</p>
      </div>



      <div className="container">
        <form onSubmit={handleSubmit1}>
          <div>
            <label htmlFor="amount">Amount:</label>
            <input
              type="text"
              id="amount"
              value={transactionAmount}
              onChange={(event) => setTransactionAmount(event.target.value)}
            />
          </div>
          <div>
            <label htmlFor="type">Type:</label>
            <select
              id="type"
              value={transactionType}
              onChange={(event) => setTransactionType(event.target.value)}
            >
              <option value="CREDIT">Credit</option>
              <option value="DEBIT">Debit</option>
            </select>
          </div>
          <button type="submit">Submit</button>
        </form>
      </div>


    

    <div className="container mx-auto mt-5">
      <table>
        <thead>
          <tr>
            <th>TransactionID</th>
            <th>Amount</th>
            <th>Description</th>
            <th>CreatedDate</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item) => (
            <tr key={item._id}>
              <td>{item._id}</td>
              <td>{item.amount}</td>
              <td>{item.description}</td>
              <td>{item.created_at}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <button onClick={handleExport}>Export to CSV</button>
    </div>







    </div>

    
  );
};

export default WalletInitialization;